use std::fs;
use std::env;
use std::path::PathBuf;
use reqwest::Client;
use reqwest::multipart::Form;
use serde_json::json;
use serde_json::Value;
use serde::{Deserialize, Serialize};
use chrono::{Local, DateTime, TimeZone, Utc};
use tokio::task;
use rand::seq::SliceRandom;
use rand::prelude::*;
use rand::Rng;
use dotenv::dotenv;


#[derive(Deserialize)]
struct ResponseValue {
    data: LoginData
}

#[derive(Deserialize)]
struct LoginData {
    original_id: i32,
    token: String,
    merchant_id: i32,
}

fn random_minute() -> i32 {
    let mut rng = rand::thread_rng();
    return rng.gen_range(0..59)
}

fn get_files(dirname: &str) -> Vec<String> {
    let mut items: Vec<String> = Vec::new();
    let dir = std::fs::read_dir(dirname);
    for item in dir.expect("fail") {
        if let Ok(item) = item {
            items.push(item.path().into_os_string().into_string().unwrap());
        }
    }
    items
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    dotenv().ok();
    let now: DateTime<Local> = Local::now();
    let senna_uri = "https://dev.senna.co.id/api/v1/merchant/login";
    let latitude = "-7.82265604";
    let longitude = "110.42711631";
    let login_body = json!({
        "email": env::var("email").unwrap(),
        "password": env::var("password").unwrap(),
        "role": "3",
        "is_from_google": "0"
    });


    let dir_items = get_files("src/assets");
    let file = dir_items.into_iter().choose(&mut rand::thread_rng()).unwrap();
    let path = std::path::Path::new(&file);
    let sub_file = std::fs::read(path)?;
    let sub_file_part = reqwest::multipart::Part::bytes(sub_file)
        .file_name(path.file_name().unwrap().to_string_lossy().into_owned())
        .mime_str("application/octet-stream")?;

    let client = reqwest::Client::new();
    let res = client
        .post(senna_uri)
        .json(&login_body)
        .send()
        .await?
        .json::<ResponseValue>()
        .await?;

    let user_id = res.data.original_id.to_string();
    let merchant_id = res.data.merchant_id.to_string();
    let token = res.data.token;

    let date = now.format("%F").to_string();
    let min = random_minute();
    let date_now = format!("{} 09:{}", date, min);

    let form = reqwest::multipart::Form::new()
            .text("md_merchant_id", merchant_id)
            .text("md_staff_user_id", user_id)
            .text("start_work", date_now)
            .text("start_latitude", latitude)
            .text("start_longitude", longitude)
            .part("start_work_file", sub_file_part);


    let res_att = client
        .post("https://dev.senna.co.id/api/v2/senna/merchant/toko/attendance/start")
        .header("senna-auth", format!("{}", token))
        .multipart(form)
        .send()
        .await?
        .json::<serde_json::Value>()
        .await?;
    
    println!("{:?}", res_att);

    Ok(())

}
